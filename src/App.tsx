import type { Component } from 'solid-js';
import Chats from './UI/components/Chats';
import Header from './UI/components/Header';
import Input from './UI/components/Input';
import NoChatSelected from './UI/components/NoChatSelected';

const App: Component = () => {
  return (
    <div class="h-screen w-screen bg-white grid place-content-center">
      <div class="bg-slate-200 shadow-md rounded-lg h-screen w-screen flex">
        <div class="w-3/12 bg-slate-400 h-full rounded-l-lg">
          <Header />
          <div class="p-2">
            <Input />
          </div>
          <Chats />
        </div>
        <div class="w-9/12 bg-slate-300 h-full rounded-r-lg">
          <NoChatSelected />
        </div>
      </div>
    </div>
  );
};

export default App;
