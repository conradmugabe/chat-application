import { Component } from 'solid-js';

const Input: Component = () => {
  return <input class="w-full p-1 rounded-lg px-3 focus:border-none active:border-none" type="text" placeholder="Search" />;
};

export default Input;
