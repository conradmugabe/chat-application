import { Component } from 'solid-js';
import image from './../../assets/image.jpg';

type Props = {
  size?: number;
};

const UserImage = ({ size }: Props) => {
  const image =
    'https://source.unsplash.com/random/1920x1080'
  return (
    <div class="h-16 w-16">
      <div
        class={`bg-no-repeat bg-cover bg-center bg-fixed rounded-full h-${size} w-${size}`}
        style={`background-image: url('${image}')`}
      />
    </div>
  );
};

export default UserImage;
