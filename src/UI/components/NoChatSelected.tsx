import { Component } from 'solid-js';
import icon from './../../assets/group_chat.svg';

const NoChatSelected: Component = () => {
  return (
    <div class="w-full h-full bg-slate-200 border-b-8 border-b-slate-700 flex flex-col justify-center items-center rounded-r-lg">
      <div class="flex flex-col mt-auto ju-center items-center gap-2">
        <img class="h-64 w-64" src={icon} alt="group chat" />
        <h3 class="text-3xl text-gray-600">Chat Application</h3>
        <div class="text-gray-500">
          <p class="text-sm text-center">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Earum
            labore aliquid quisquam nesciunt?
          </p>
          <p class="text-sm text-center">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Earum
            labore?
          </p>
        </div>
      </div>
      <p class="mt-auto mb-10 text-gray-600">End-to-end encrypted</p>
    </div>
  );
};

export default NoChatSelected;
