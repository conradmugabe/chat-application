import { Component } from 'solid-js';
import { RiCommunicationChatNewLine, RiSystemMore2Fill } from 'solid-icons/ri';
import UserImage from './UserImage';

const Header: Component = () => {
  return (
    <div class="bg-slate-300 flex items-center p-2">
      <div class="cursor-pointer">
        <UserImage size={8} />
      </div>
      <div class="flex ml-auto">
        <div class="cursor-pointer p-4 rounded-full hover:bg-slate-200">
          <RiCommunicationChatNewLine size={20} />
        </div>
        <div class="cursor-pointer p-4 rounded-full hover:bg-slate-200">
          <RiSystemMore2Fill size={20} />
        </div>
      </div>
    </div>
  );
};

export default Header;
