import { Component } from 'solid-js';
import Chat from './Chat';

const Chats: Component = () => {
  return (
    <div class="flex flex-col overflow-y-auto h-full">
      <Chat />
      <Chat />
      <Chat />
      <Chat />
    </div>
  );
};

export default Chats;
