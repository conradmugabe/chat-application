import { Component } from 'solid-js';
import UserImage from './UserImage';

const Chat: Component = () => {
  return (
    <div class="w-full bg-slate-300 cursor-pointer p-2 hover:bg-slate-200 flex items-center gap-4 relative">
      <div
        class="bg-gray-400 absolute left-20 w-3/4 bottom-0"
        style={{ height: '1px' }}
      />
      <UserImage />
      <div class="flex justify-between w-full h-full">
        <div>
          <p class="text-base font-semibold text-gray-600">Conrad Mugabe</p>
          <p class="text-sm text-gray-500">Hello World!</p>
        </div>
        <p class="text-xs">yesterday</p>
      </div>
    </div>
  );
};

export default Chat;
